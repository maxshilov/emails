import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    APP_ROOT = basedir
    CSRF_ENABLED = True
    WTF_CSRF_SECRET_KEY = "dsofpkoasodksap"
    SECRET_KEY = 'secret'
    SQLALCHEMY_DATABASE_URI = "sqlite:///"+APP_ROOT+"/test.db"
    MANDRILL_API_KEY = "LQP-N0wvFNUJjpY2VuEZHw"
    CELERY_BROKER_URL = 'amqp://guest:guest@localhost:5672//'
    CELERY_RESULT_BACKEND = 'amqp://guest:guest@localhost:5672//'
#   CELERY_ACKS_LATE = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class ProductionConfig(Config):
    DEBUG = False


class DevelopConfig(Config):
    TO_EMAIL = "mshilov@gmail.com"
    FROM_EMAIL = TO_EMAIL
    DEBUG = True
    ASSETS_DEBUG = True
