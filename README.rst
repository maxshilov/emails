Installation
^^^^^^^^^^^^

1. Install python dependencies

    $ pip install -r requirements.txt

2. Install ``rabbitmq`` or any other message broker

    $ apt-get install rabbitmq-server

3. Edit ``config.py`` configuration according to your environment.

4. Init database

    $ python manage.py db init
    $ python manage.py db migrate
    $ python manage.py db upgrade

5. Run celery

    $ celery -A email_app.celery worker --loglevel debug

6. Run debug server.

    $ python email_app.py

7. Open http://localhost:5000/api/v1.0/save_email?event_id=123&subject=subject&content=email&timestamp=Tue,%2004%20Nov%202015%2014:09:37%20-00700

 Email message will be sent 4 Nov 2015 14:09 UTC-7 timezone.



