from email_app import db
from datetime import datetime


class Email(db.Model):
    __tablename__ = 'emails'
    __table_args__ = {'sqlite_autoincrement': True}

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    event_id = db.Column(db.Integer, index=True, nullable=False)
    subject = db.Column(db.UnicodeText(length=1000), nullable=False)
    content = db.Column(db.UnicodeText, nullable=False)
    timestamp = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        return "<Email id: %s event_id: %s timestamp: %s>" % (self.id,
                                                              self.event_id,
                                                              self.timestamp)
