from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from celery import Celery

app = Flask(__name__)
app.config.from_object('config.DevelopConfig')
db = SQLAlchemy(app)


def make_celery(app):
    cel = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    cel.conf.update(app.config)
    return cel

# initializations
celery = make_celery(app)


from emails.views import *

if __name__ == '__main__':
    app.run(host="0.0.0.0")
