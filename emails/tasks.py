from mandrill import Mandrill, Error
from email_app import make_celery, app, db
from models import Email


celery = make_celery(app)


@celery.task
def send_one_email_task(email_id):
    print "enter task"
    email = db.session.query(Email).filter(Email.id == email_id).first()
    if not email:
        app.log_exception("email (%s) not found" % email_id)
        return
    mc = Mandrill(app.config['MANDRILL_API_KEY'])
    message = {'from_email': app.config['FROM_EMAIL'],
               'subject': email.subject,
               'to': [{'email': app.config['TO_EMAIL']}],
               'html': email.content,
               }
    try:
        return mc.messages.send(message)
    except Error, e:
        print "errror"
        app.log_exception(e)
