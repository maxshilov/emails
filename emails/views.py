from flask.ext.restful import reqparse, fields, marshal_with, Resource, Api
from dateutil import parser
import pytz

from email_app import db, app
import tasks, models


marshall_fields = {
    'event_id': fields.Integer,
    'subject': fields.String,
    'content': fields.String,
    'timestamp': fields.DateTime
}


# validate provided  datetime string
def datetime_string(value):
    try:
        x = parser.parse(value)
    except (TypeError, ValueError), e:
        raise ValueError("Invalid datetime string: %s. Example: Fri, 03 Nov 2017 17:26:53 -0000" % e)

    return x


class EmailAPI(Resource):

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('event_id', required=True, type=int,
                                   location='values', help='No event_id provided')
        self.reqparse.add_argument('subject', required=True, type=unicode,
                                   location='values', help='No subject provided')
        self.reqparse.add_argument('content', required=True, type=unicode,
                                   location='values', help='No content provided')
        self.reqparse.add_argument('timestamp', type=datetime_string,
                                   location='values', help='No send_at provided')
        super(EmailAPI, self).__init__()

    @marshal_with(marshall_fields)
    def post(self):
        args = self.reqparse.parse_args()
        message = models.Email(event_id=args['event_id'],
                               subject=args['subject'],
                               content=args['content'],
                               timestamp=args['timestamp'].astimezone(pytz.utc) if args['timestamp'] else None)

        db.session.add(message)
        db.session.commit()
        # Create celery task. eta always in utc
        tasks.send_one_email_task.apply_async(args=[message.id], eta=message.timestamp)
        return message, 201

    # only for debugging
    def get(self):
        return self.post()

api = Api(app, prefix='/api/v1.0')
api.add_resource(EmailAPI, '/save_email')
